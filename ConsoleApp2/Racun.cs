﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp2
{
    class Racun<T> : IRacun{

        private DateTime izdavanjeRacuna { get; set; }
        private int IznosRacuna { get; set; }
        public decimal DohvatiIznos() { return Convert.ToDecimal(IznosRacuna); }
        public DateTime DohvatiDatumIzdavanja() { return izdavanjeRacuna; }


        public Racun(DateTime izdavanjeRacuna,int IznosRacuna) {

            IznosRacuna = IznosRacuna;
            izdavanjeRacuna = DateTime.UtcNow;

            if (IznosRacuna < 10)
            {
            
                throw new Exception("Premali iznos");

            }
           
        }
    }
}
